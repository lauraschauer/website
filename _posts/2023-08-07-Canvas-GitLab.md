---
layout: post
title: 'A Canvas-GitLab Integration'
description: 'An Athena Swan Project'
image: '/images/projects/canvas-gitlab/logo-canvas-gitlab.jpg'
intro_image: '/images/projects/canvas-gitlab/logo-canvas-gitlab.jpg'
showcase: true
---

Image you're a **computer science lecturer**, and you're using [Canvas](https://www.instructure.com/canvas) and [GitLab](https://about.gitlab.com/) for the organisation of your courses. Both systems are great, but wouldn't it be nice if they could somehow talk to each other? If you didn't have to download interaction data from both systems and combine them manually. Or to search for each student's GitLab project to find their submission code. 

This is the start of a solution to problems like these. 

## An integration between Canvas and GitLab

<img src="{{ site.url }}/images/projects/canvas-gitlab/logo-athena-swan.jpg" alt="Athena Swan Logo" width="200rem" style="float: right;">

The canvas-gitlab integration project started out as my dissertation project, supervised by [Rob Stewart](https://www.macs.hw.ac.uk/~rs46/), for which Heriot-Watt awarded me with the Cooper-Walker Engineering Ltd Prize for outstanding project work. I was also shortlisted for [2023's Young Software Engineers Award](https://www.scotlandis.com/blog/2023s-young-software-engineers-of-the-year-revealing-scotlands-best-graduate-talent/) alongside eight other impressive dissertations.

During summer, I was able to do (a lot) more work on the project thanks to [Athena Swan](https://www.hw.ac.uk/uk/services/equality-diversity/athena-swan/about-athena-swan.htm). Their sponsorship enabled me to continue working on the project after graduation, and I wholeheartedly recommend applying to their summer bursaries. The [charter](https://www.advance-he.ac.uk/equality-charters/athena-swan-charter) aims for gender equality within higher education and research, and do a great job at supporting women in STEMM. As someone in this category of people, I really appreciate their effort. 

Moreover, this bursary enabled me to write a paper about this project with [Rob Stewart](https://www.macs.hw.ac.uk/~rs46/) and [Manuel Mareek](https://www.macs.hw.ac.uk/~mm894/), which is about to be submitted to [ICSE 24](https://conf.researchr.org/track/icse-2024/icse-2024-software-engineering-education-and-training-track). 

## Canvas-gitlab

---

If you want to create your own integration use case, take [this](https://lauraschauer.gitlab.io/2023/08/16/Canvas-GitLab-Use-Case/) blog post as a starting point. I walk you through the creation of the *Submission Logs* use case, which adds the URL of each student's GitLab project to Canvas, so that lecturers don't have to go searching for it. 

---

The integration is written in Haskell, and builds on two Haskell libraries to function: 

1. [gitlab-haskell](https://gitlab.com/robstewart57/gitlab-haskell): The library covering the GitLab API
2. [canvas-haskell](https://gitlab.com/lauraschauer/canvas-haskell-library): My library covering the Canvas API 

<img src="{{ site.url }}/images/projects/canvas-gitlab/system-overview.jpg" alt="System Overview" width="500vw" style="float: left; display: inline-block; padding-bottom: 1rem; padding-right: 2rem;">

Both libraries take over sending and receiving HTTP requests. This means that you can use the libraries' functions instead of having to code your own HTTP requests to send to the Canvas or the GitLab API.  

They also deal with pagination. A paginated API endpoint returns the data in chunks (pages), and returns some link with each page that points to the next one (like a linked list). Both gitlab-haskell and canvas-haskell have an in-built functionality that loops through the pages and returns all data, without the developer having to write complicated loops. 

The image on the left shows an overview of how everything is working together. All you need to do is
```haskell
import Canvas
import GitLab
```
at the beginning of your file, and voilà — access to all the data you need. 

There are — as of now — 5 use cases included in canvas-gitlab. Each of them makes one aspect of using both Canvas and GitLab together easier. Three of them are labelled as `client-side`, which means that they are manually started, whereas `server-side` use cases are triggered by GitLab hooks on the Heriot-Watt GitLab server.

### 1. Coursework Deadline Reminders

This application of canvas-gitlab sends deadline reminders to students in the form of an issue on their GitLab project. The program is run for one course and one assignment on that course at a time. It is possible to only target students of a certain course section. The text of the GitLab issue can be freely selected by lecturers. 

### 2. Canvas Submission Comment

This application of canvas-gitlab creates a comment on Canvas when a student pushes code to their GitLab project. The comment shows up on the canvas assignment and is visible to lecturers only until the deadline. It contains the number of commits that have been included in the push, as well as the URL to the student's GitLab project.

### 3. Multi Course Student Analytics

The third application of canvas-gitlab helps lecturers with identifying students who are not engaging. It retrieves last login data from both Canvas and GitLab for all students of one or more courses and combines the information in a spreadsheet. This is a read-only program that will not create any changes on Canvas or GitLab.

### 4. Single Course Student Analytics

This application of canvas-gitlab also helps lecturers with identifying students who are not engaging. It generates a spreadsheet that gives insight into students' activity on one course only. The data is more detailed and includes information about each assignment on the course. This is a read-only program that will not create any changes on Canvas or GitLab.

### 5. Profile Update

This implementation of \textit{Canvas-GitLab} retrieves a user's Profile information from Canvas and adds it to their GitLab profile. The program runs at the course level and updates the profiles of all users on the course, including students, lectureres, TAs, and others as long as they have an account on both platforms. In particular, it will add the following to a GitLab profile:

1. The bio of a Canvas account if set.
2. The user's pronouns if set.
3. The user's location based on the sections they are part of.
